import 'dotenv/config';
import express from 'express';
import 'express-async-errors';
import logger from './common/logger.js';

const app = express();
const port = process.env.PORT || 3000;

app.get('/', (req, res) => {
  res.send('Welcome to Phantom Chat!');
});

app.use((err, req, res, _next) => {
  logger.error(err.stack);
  res.status(500).send('something went wrong');
});

app.listen(port, () => {
  logger.info(`Phantom Chat server is running on port ${port}`);
});
